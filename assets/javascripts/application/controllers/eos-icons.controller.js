/* ==========================================================================
  Icons version and links
  ========================================================================== */

/* Prepare local variables that will be reused in different methods */
let $iconsContainer, $iconDisplayTemplate, $tagTemplate, $animatedIconDisplayTemplate

$(function () {
  // Prepare icons containers
  $('.js-eos-icons-notification').hide()
  $iconsContainer = $('.js-eos-icons-list')
  $iconDisplayTemplate = $('.js-icon-display').clone(true)
  $('.js-icon-display').remove()
  $animatedIconDisplayTemplate = $('.js-animated-icon-display').clone(true)
  $('.js-animated-icon-display').remove()

  if ($('.js-eosIconsSet').length) {
    // Send an empty search to return the full collection of icons
    getIconsCollection('')
  }

  // Tag span clone
  $tagTemplate = $('.js-eos-icons-tag').clone(true)

  /* Better search response and API call
  ========================================================================== */
  let typingTimer
  const doneTypingInterval = 800

  /* on keyup, start the countdown */
  $('.js-eos-icon-filter').on('keyup', function () {
    $('.js-eos-icons-notification').hide()
    clearTimeout(typingTimer)
    typingTimer = setTimeout(doneTyping, doneTypingInterval)
  })

  /* on keydown, clear the countdown */
  $('.js-eos-icon-filter').on('keydown', function () {
    clearTimeout(typingTimer)
  })

  /* once typing is done */
  function doneTyping () {
    const query = $('.js-eos-icon-filter').val()
    onSearch(query)
  }
})

const onSearch = (q = '') => {
  $('.js-icon-display').remove()
  $('.js-animated-icon-display').remove()
  getIconsCollection(q)
}

const renderIcons = (collection) => {
  for (let i = 0; i < collection.length; i++) {
    const newIconDisplay = collection[i].animatedClass ? $animatedIconDisplayTemplate.clone(true) : $iconDisplayTemplate.clone(true)
    const iconName = collection[i].name
    // Add icon name
    $(newIconDisplay).find('.js-animated-eos-icons').attr('src', `/images/animated-icons/${iconName}-gray.svg`)
    $(newIconDisplay).find('.js-icon-name').text(iconName)
    $($iconsContainer).append(newIconDisplay)
    /* Attach the click event on creation */
    $(newIconDisplay).on('click', function () {
      toggleEosIconInPanel(iconName)
    })
  }
}

const toggleEosIconInPanel = (iconName) => {
  $('.js-eos-icons-name').text(iconName)

  /* Get information for the Do and Dont and the Tags */
  getMoreInfoFromIconService(iconName, function (data) { // eslint-disable-line no-undef
    const notFoundMsg = 'No information available'
    const animatedClass = data ? data.animatedClass || null : null
    const textDo = data ? data.do || notFoundMsg : notFoundMsg
    const textDont = data ? data.dont || notFoundMsg : notFoundMsg
    const tags = data.tags !== undefined ? data.tags : ' '

    addIconData(textDo, textDont, tags)

    if (animatedClass) {
      $('.js-eos-icons-code-example').addClass('d-none')
      $('.js-eos-animation-icons-code-example').removeClass('d-none')
      addAnimatedIconSpecificData(iconName) // eslint-disable-line no-undef
    } else {
      $('.js-eos-icons-code-example').removeClass('d-none')
      $('.js-eos-animation-icons-code-example').addClass('d-none')
    }
  })
}

const addIconData = (textDo, textDont, tags) => {
  $('.js-eos-icons-do').html(jQuery.parseHTML(textDo))
  $('.js-eos-icons-dont').html(jQuery.parseHTML(textDont))

  $('.js-eos-icons-tag').remove()
  for (let i = 0; i < tags.length; i++) {
    const _tagTemplate = $tagTemplate.clone(true)
    $(_tagTemplate).text(tags[i])
    $('.js-eos-icons-tags-list').append(_tagTemplate)
  }
}

/* Declared functions
========================================================================== */
/*
 In order to access this function from Unit Testing, we need to make it a declared function
 dont change this to arrow function or unit test will break
*/

// don't use the arrow function here
function getIconsCollection (q) {
  getIconsService(q, function (searchResult) { // eslint-disable-line no-undef
    const iconsMerged = searchResult
    renderIcons(iconsMerged)
  })
}
